import React from 'react';
import ReactDOM from 'react-dom';
import App from './app/';
import './font-awesome'
import './../scss/styles.scss';

const InitApp = () => (
  <App />
);

ReactDOM.render(<InitApp />, document.getElementById('connect-four-app'));
