export const clearBoardState = [
  [{ value: '' }, { value: '' }, { value: '' }, { value: '' }, { value: '' }, { value: '' }, { value: '' }],
  [{ value: '' }, { value: '' }, { value: '' }, { value: '' }, { value: '' }, { value: '' }, { value: '' }],
  [{ value: '' }, { value: '' }, { value: '' }, { value: '' }, { value: '' }, { value: '' }, { value: '' }],
  [{ value: '' }, { value: '' }, { value: '' }, { value: '' }, { value: '' }, { value: '' }, { value: '' }],
  [{ value: '' }, { value: '' }, { value: '' }, { value: '' }, { value: '' }, { value: '' }, { value: '' }],
  [{ value: '' }, { value: '' }, { value: '' }, { value: '' }, { value: '' }, { value: '' }, { value: '' }]
]
export const horizontalWinState = [
  [{ value: '' }, { value: '' }, { value: '' }, { value: '' }, { value: '' }, { value: '' }, { value: '' }],
  [{ value: '' }, { value: '' }, { value: '' }, { value: '' }, { value: '' }, { value: '' }, { value: '' }],
  [{ value: '' }, { value: '' }, { value: '' }, { value: '' }, { value: '' }, { value: '' }, { value: 'black' }],
  [{ value: '' }, { value: '' }, { value: '' }, { value: '' }, { value: '' }, { value: '' }, { value: 'black' }],
  [{ value: '' }, { value: '' }, { value: '' }, { value: '' }, { value: '' }, { value: '' }, { value: 'black' }],
  [{ value: '' }, { value: '' }, { value: '' }, { value: 'red' }, { value: 'red' }, { value: 'red' }, { value: 'red' }]
]
export const verticalWinState = [
  [{ value: '' }, { value: '' }, { value: '' }, { value: '' }, { value: '' }, { value: '' }, { value: '' }],
  [{ value: '' }, { value: '' }, { value: '' }, { value: '' }, { value: '' }, { value: '' }, { value: '' }],
  [{ value: '' }, { value: '' }, { value: '' }, { value: '' }, { value: '' }, { value: '' }, { value: 'red' }],
  [{ value: '' }, { value: '' }, { value: '' }, { value: '' }, { value: '' }, { value: '' }, { value: 'red' }],
  [{ value: '' }, { value: '' }, { value: '' }, { value: '' }, { value: '' }, { value: '' }, { value: 'red' }],
  [{ value: '' }, { value: '' }, { value: '' }, { value: 'black' }, { value: 'black' }, { value: 'black' }, { value: 'red' }]
]
export const diagonalUpRightWinState = [
  [{ value: '' }, { value: '' }, { value: '' }, { value: '' }, { value: '' }, { value: '' }, { value: '' }],
  [{ value: '' }, { value: '' }, { value: '' }, { value: '' }, { value: '' }, { value: '' }, { value: '' }],
  [{ value: '' }, { value: '' }, { value: '' }, { value: 'black' }, { value: '' }, { value: '' }, { value: '' }],
  [{ value: '' }, { value: '' }, { value: 'black' }, { value: 'red' }, { value: '' }, { value: '' }, { value: '' }],
  [{ value: '' }, { value: 'black' }, { value: 'black' }, { value: 'red' }, { value: '' }, { value: '' }, { value: '' }],
  [{ value: 'black' }, { value: 'red' }, { value: 'red' }, { value: 'red' }, { value: '' }, { value: '' }, { value: '' }]
]
export const diagonalDownRightWinState = [
  [{ value: '' }, { value: '' }, { value: '' }, { value: '' }, { value: '' }, { value: '' }, { value: '' }],
  [{ value: '' }, { value: '' }, { value: '' }, { value: '' }, { value: '' }, { value: '' }, { value: '' }],
  [{ value: 'black' }, { value: '' }, { value: '' }, { value: '' }, { value: '' }, { value: '' }, { value: '' }],
  [{ value: 'red' }, { value: 'black' }, { value: '' }, { value: '' }, { value: '' }, { value: '' }, { value: '' }],
  [{ value: 'red' }, { value: 'red' }, { value: 'black' }, { value: '' }, { value: '' }, { value: '' }, { value: '' }],
  [{ value: 'red' }, { value: 'black' }, { value: 'red' }, { value: 'black' }, { value: '' }, { value: '' }, { value: '' }]
]