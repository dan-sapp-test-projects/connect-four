const checkForWinner = (setWinner, boardState) => {
  let i, j, verticalEntry, horizontalEntry, diagDownRightEntry, diagUpRightEntry
  let verticalCounter = 0
  let horizontalCounter = 0
  let diagDownRightCounter = 0
  let diagUpRightCounter = 0
  let verticalColor = boardState[0][0].value
  let horizontalColor = boardState[0][0].value
  let diagDownRightColor = boardState[0][0].value
  let diagUpRightColor = boardState[0][0].value
  let longestChain = 0

  // i and j are not great var names but each scenario requires them to be used differently 
  for (i = 0; i <= 6; i++) {
    for (j = 0; j <= 6; j++) {

      // check for vertical win
      if (i <= 5) {
        verticalEntry = boardState[i][j]
        if (verticalEntry.value !== '' && verticalEntry.value === verticalColor) {
          verticalCounter++
          if (verticalCounter > longestChain) {
            longestChain = verticalCounter
            if (longestChain >= 4) {
              setWinner(verticalColor)
            }
          }
        } else {
          verticalCounter = 1
        }
        verticalColor = verticalEntry.value
      }

      // check for horizontal win
      if (j <= 5) {
        horizontalEntry = boardState[j][i]
        if (horizontalEntry.value !== '' && horizontalEntry.value === horizontalColor) {
          horizontalCounter++
          if (horizontalCounter > longestChain) {
            longestChain = horizontalCounter
            if (longestChain >= 4) {
              setWinner(horizontalColor)
            }
          }
        } else {
          horizontalCounter = 1
        }
        horizontalColor = horizontalEntry.value
      }

      // check for diagonal down-right win
      if (i <= 5 && i + j >= 3 && (i + j <= 8)) {
        diagDownRightEntry = boardState[i + j - 3][j]
        if (diagDownRightEntry.value !== '' && diagDownRightEntry.value === diagDownRightColor) {
          diagDownRightCounter++
          if (diagDownRightCounter > longestChain) {
            longestChain = diagDownRightCounter
            if (longestChain >= 4) {
              setWinner(diagDownRightColor)
            }
          }
        } else {
          diagDownRightCounter = 1
        }
        diagDownRightColor = diagDownRightEntry.value
      }

      // check for diagonal up-right win
      if (i <= 5 && (i - j + 3) >= 0 && (i - j + 3) <= 5) {
        diagUpRightEntry = boardState[i - j + 3][j]
        if (diagUpRightEntry.value !== '' && diagUpRightEntry.value === diagUpRightColor) {
          diagUpRightCounter++
          if (diagUpRightCounter > longestChain) {
            longestChain = diagUpRightCounter
            if (longestChain >= 4) {
              setWinner(diagUpRightColor)
            }
          }
        } else {
          diagUpRightCounter = 1
        }
        diagUpRightColor = diagUpRightEntry.value
      }
    }
  }
}

export default checkForWinner