import React, { useEffect } from 'react';
import { Modal, Button } from 'react-bootstrap'

const StartModal = ({ startModalIsOpen, setActivePlayerColor, setPlayer1Color, setStartModalIsOpen }) => (
  <Modal show={startModalIsOpen} onHide={() => { }}>
    <Modal.Body>Player 1 - Please Choose your Color:</Modal.Body>
    <Modal.Footer>
      <div className="btn-wrapper">
        <Button className="color-btn red"
          onClick={() => {
            setPlayer1Color('red')
            setActivePlayerColor('red')
            setStartModalIsOpen(false)
          }}>
          RED
        </Button>
        <Button className="color-btn black"
          onClick={() => {
            setPlayer1Color('black')
            setActivePlayerColor('black')
            setStartModalIsOpen(false)
          }}>
          BLACK
        </Button>
      </div>
    </Modal.Footer>
  </Modal>
)

export default StartModal 