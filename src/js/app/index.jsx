import React, { useState } from 'react'
import Board from './board'
import DropRow from './drop-row'
import NewPiece from './new-piece'
import StartModal from './start-modal'
import WinnerModal from './winner-modal'
import checkForWinner from '../checkForWinner'
// import {clearBoardState} from '../mockBoardStates'

const App = () => {
  const initialBoardState = [
    [{ value: '' }, { value: '' }, { value: '' }, { value: '' }, { value: '' }, { value: '' }, { value: '' }],
    [{ value: '' }, { value: '' }, { value: '' }, { value: '' }, { value: '' }, { value: '' }, { value: '' }],
    [{ value: '' }, { value: '' }, { value: '' }, { value: '' }, { value: '' }, { value: '' }, { value: '' }],
    [{ value: '' }, { value: '' }, { value: '' }, { value: '' }, { value: '' }, { value: '' }, { value: '' }],
    [{ value: '' }, { value: '' }, { value: '' }, { value: '' }, { value: '' }, { value: '' }, { value: '' }],
    [{ value: '' }, { value: '' }, { value: '' }, { value: '' }, { value: '' }, { value: '' }, { value: '' }]
  ]
  const [player1Color, setPlayer1Color] = useState('')
  const [activePlayerColor, setActivePlayerColor] = useState('')
  const [startModalIsOpen, setStartModalIsOpen] = useState(true)
  const [turn, setTurn] = useState(1)
  const [winner, setWinner] = useState()
  const [boardState, setBoardState] = useState(initialBoardState)

  const resetGame = () => {
    setStartModalIsOpen(true)
    setBoardState(initialBoardState)
    setWinner()
    setTurn(1)
  }

  const onDrop = (column) => {
    const newBoardState = boardState.map(row => row)
    const emptiesInColumn = boardState.reduce((acc, row) => {
      if (row[column].value === '') {
        return acc + 1
      }
      return acc
    }, 0)
    if (emptiesInColumn === 0) return
    const rot = Math.floor(Math.random() * 360)
    newBoardState[emptiesInColumn - 1].splice(column, 1, { value: activePlayerColor, rot });
    setBoardState(newBoardState)
    if (activePlayerColor === 'red') {
      setActivePlayerColor('black')
    } else {
      setActivePlayerColor('red')
    }
    if (turn >= 7) {
      checkForWinner(setWinner, boardState)
    }
    setTurn(turn + 1)
  }

  if (turn === 43) {
    setWinner('tie')
  }
  return (
    <div className="container">
      <StartModal
        startModalIsOpen={startModalIsOpen}
        setActivePlayerColor={setActivePlayerColor}
        setPlayer1Color={setPlayer1Color}
        setStartModalIsOpen={setStartModalIsOpen} />
      <WinnerModal
        winner={winner}
        setWinner={setWinner}
        resetGame={resetGame} />
      <div className="board-wrapper">
        <DropRow onDrop={onDrop} />
        <Board boardState={boardState} />
      </div>
      <div className="side-board-wrapper">
        <NewPiece
          activePlayerColor={activePlayerColor}
          turn={turn}
          player1Color={player1Color} />
      </div>
    </div>
  )
}

export default App 