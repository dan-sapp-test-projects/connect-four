import React from 'react'

const DropRow = ({ onDrop }) => (
  <div className="drop-row">
    {[1, 2, 3, 4, 5, 6, 7].map((a, index) => (
      <div
        className="drop-slot"
        key={`drop-slot-${index}`}
        onDrop={() => onDrop(index)}
        onDragOver={(e) => e.preventDefault()} />
    ))
    }
  </div>
)
export default DropRow