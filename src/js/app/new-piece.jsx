import React from 'react'
import Chip from './chip'

const NewPiece = ({ activePlayerColor, player1Color, turn }) => (
  <div className="piece-wrapper">
    <div>
      Drag and drop your piece above the board
    </div>
    <Chip draggable={true} value={activePlayerColor} />
    <div className="player-text">{`Player ${activePlayerColor === player1Color ? '1' : '2'}`}</div>
    <div className="turn-text">Turn {turn}</div>
  </div>
)

export default NewPiece