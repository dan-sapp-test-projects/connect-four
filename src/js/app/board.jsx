import React from 'react'
import Chip from './chip'

const Board = ({ boardState }) => (
  <div className="board">
    {
      boardState.map((row, rowIndex) => (
        <div key={`row-${rowIndex}`} className="board-row">
          {row.map((slot, index) => (
            <Chip key={`entry-${rowIndex}-${index}`} value={slot.value} rot={slot.rot} />
          ))
          }
        </div>
      ))
    }
  </div>
)

export default Board