import React from 'react'
import styled from 'styled-components'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

const Chip = ({ value, rot = 0, draggable }) => {
  const ChipComp = styled.div`
    transform: ${props => {
      return `rotate(${rot}deg);`
    }}
  `
  switch (value) {
    case 'red':
      return (<ChipComp className="chip red" draggable={draggable}>
        <FontAwesomeIcon icon={['fas', 'crown']} />
      </ChipComp>)
    case 'black':
      return (<ChipComp className="chip black" draggable={draggable}>
        <FontAwesomeIcon icon={['fas', 'crown']} />
      </ChipComp>)
    default:
      return (<div className="chip" />)
  }
}

export default Chip