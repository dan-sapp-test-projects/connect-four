import React from 'react';
import { Modal, Button } from 'react-bootstrap'

const WinnerModal = ({ resetGame, winner }) => (
  <Modal show={!!winner} onHide={() => { }}>
    {winner === 'tie'
      ? <Modal.Body>TIE game!</Modal.Body>
      : <Modal.Body>{winner && winner.toUpperCase()} Player Wins!</Modal.Body>
    }
    <Modal.Footer>
      <div className="single-btn-wrapper">
        <Button className={`color-btn ${winner}`}
          onClick={() => resetGame()}>
          OK
        </Button>
      </div>
    </Modal.Footer>
  </Modal>
)

export default WinnerModal 