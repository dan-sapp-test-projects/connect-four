import checkForWinner from './checkForWinner';
import {clearBoardState,
  horizontalWinState,
  verticalWinState,
  diagonalUpRightWinState,
  diagonalDownRightWinState
} from './mockBoardStates'

describe('Checking for winner', () => {
  it('should NOT find a winner', () => {
    let winner
    const setWinner = (color) => {
      winner = color
    }
    checkForWinner(setWinner, clearBoardState)
    expect(winner).toEqual(undefined)
  })
  it('should find a horizontal winner', () => {
    let winner
    const setWinner = (color) => {
      winner = color
    }
    checkForWinner(setWinner, horizontalWinState)
    expect(winner).toEqual('red')
  })
  it('should find a vertical winner', () => {
    let winner
    const setWinner = (color) => {
      winner = color
    }
    checkForWinner(setWinner, verticalWinState)
    expect(winner).toEqual('red')
  })
  it('should find a diagonal down-right winner', () => {
    let winner
    const setWinner = (color) => {
      winner = color
    }
    checkForWinner(setWinner, diagonalDownRightWinState)
    expect(winner).toEqual('black')
  })
  it('should find a diagonal up-right winner', () => {
    let winner
    const setWinner = (color) => {
      winner = color
    }
    checkForWinner(setWinner, diagonalUpRightWinState)
    expect(winner).toEqual('black')
  })
})