const config = require('./webpack.config');
const path = require('path');

config.devtool = false;
config.entry = ['./src/js/index.jsx'];
config.output = {
  path: path.resolve(__dirname, './dist/connect-four'),
  filename: 'main.js',
  library: 'connect-four',
  libraryTarget: 'umd',
  umdNamedDefine: true
};
module.exports = config;